<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH BANNER

\*----------------------------------------------------------------*/
?>

<?php $bg = get_field('banner_background_image'); ?>
<section class="banner" style="background-image: url('<?php echo $bg['sizes']['xlarge']; ?>');">
	<div>
		<?php if ( get_field('banner_title') ) : ?>
			<h3><?php the_field('banner_title'); ?></h3>
		<?php endif; ?>
		<?php if ( get_field('banner_description') ) : ?>
			<p><?php the_field('banner_description'); ?></p>
		<?php endif; ?>
		<?php if ( have_rows('banner_buttons') ) : $i = 1; ?>
			<div class="buttons">
				<?php while( have_rows('banner_buttons') ) : the_row(); ?>
					<?php $button = get_sub_field('button'); ?>
					<?php if ( $i == 2 ) :
						$class = 'is-primary';
					else :
						$class = 'is-tertiary';
					endif; ?>
					<a class="button <?php echo $class; ?> is-large" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
				<?php $i++; endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</section>