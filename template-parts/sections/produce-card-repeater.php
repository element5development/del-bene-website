<?php 
/*----------------------------------------------------------------*\

	PRODUCE CARD REPEATER SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('produce_card_repeater') ): ?>
<section class="produce-card-repeater">
	<div>
		<?php while ( have_rows('produce_card_repeater') ) : the_row(); ?>
			<article class="preview-produce-card item">
				<?php $image = get_sub_field('image'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />

				<h3><?php the_sub_field('title'); ?></h3>

				<div class="fade-wrap">
					<p><?php the_sub_field('description'); ?></p>
					<div class="fade"></div>
				</div>
				<button class="is-ghost continue">Continue Reading</button>
			</article>
		<?php endwhile; ?>

		<?php $link = get_field('produce_card_button'); ?>
		<?php if( $link ): ?>
			<a class="button is-secondary is-large" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
</section>
<?php endif; ?>