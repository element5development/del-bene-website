<div id="fullscreen">
	<div class="section contents we-feed">
		<div class="block-contain">
			<div class="overlay"></div>
			<div class="block">
				<h1>We Feed Memories.</h1>
				<p>Del Bene Produce equips the top chefs in Michigan with superior produce, elusive ingredients, and personal attention.</p>
				<a href="<?php echo get_home_url(); ?>/products/" class="button is-secondary">Find out how</a>
			</div>
		</div>
	</div>
	<div class="section contents why-del">
		<div class="block-contain">
			<div class="block">
				<h2>WHY DEL BENE
					<br/>PRODUCE?</h2>
				<p>Del Bene Produce is committed to delivering distinctive premium products and unmatched customer service to local chefs. We focus on building relationships with our customers that goes past “just filling an order”, We help our customers grow by helping plan menus, managing food costs, and integrating tech-friendly solutions.</p>
				<a href="<?php echo get_home_url(); ?>/contact/" class="button is-tertiary">Contact Us</a>
			</div>
		</div>
	</div>
	<div class="section contents choose-del">
		<div class="overlay left"></div>
		<div class="overlay right"></div>
		<div class="block-contain">
			<div class="block">
				<h3>DETROIT’S FINEST RESTAURANTS</h3>
				<h2>CHOOSE DEL BENE</h2>
				<div class="companies">
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
					<div class="company">
						<img class="swap">
						<img class="swap">
					</div>
				</div>
				<a href="<?php echo get_home_url(); ?>/contact/" class="button is-primary">Contact Us</a>
			</div>
		</div>
	</div>
	<div class="section contents in-season">
		<div class="block-contain">
			<div class="block">
				<h2>WHAT’S IN SEASON?</h2>
				<a target="_blank" href="<?php the_field('market_report'); ?>" class="button is-primary is-ghost in-line">READ THE MARKET REPORT</a>
				<a target="_blank" href="<?php the_field('michigan_farms_list'); ?>" class="button is-primary is-ghost in-line">Michigan Farms List</a>
				<div class="item">
					<svg width="82" height="86" xmlns="http://www.w3.org/2000/svg">
						<g fill="none" fill-rule="evenodd" opacity=".66">
							<path d="M9.182 76.437a4.405 4.405 0 0 1-3.812-4.108 4.39 4.39 0 0 1 3.232-4.56 48.04 48.04 0 0 0 19.41-10.73c7.836-7.138 13.528-17.005 16.918-29.325.548-1.992 1.637-4.95 3.712-7.738a2.493 2.493 0 1 1 4.002 2.977c-1.259 1.692-2.29 3.852-2.905 6.084-7.833 28.469-25.788 38.942-37.313 42.779 17.369 1.91 31.566-1.85 42.229-11.193 3.996-3.502 9.515-9.637 13.116-18.974 1.942-5.032 2.89-9.97 2.672-13.907a2.494 2.494 0 0 1 4.98-.275c.257 4.626-.808 10.3-2.998 15.976-3.967 10.286-10.066 17.06-14.483 20.931-5.496 4.816-11.944 8.336-19.164 10.463-8.752 2.578-18.708 3.116-29.59 1.6h-.006z" fill="#FF3535"/>
							<path d="M58.182 28.75h.003-.003zm1.93 1.56a2.345 2.345 0 0 1-.515-.133 2.41 2.41 0 0 1-1.422-1.445c-.136-.305-1.728-3.648-6.905-4.729a2.494 2.494 0 0 1 1.019-4.882c5.039 1.052 7.855 3.735 9.28 5.658 2.375-.645 6.51-1.19 11.9.367a2.494 2.494 0 0 1-1.385 4.792c-6.467-1.87-10.279.001-10.438.082a2.501 2.501 0 0 1-1.535.29z" fill="#5BC434"/>
							<path d="M72.575 29.934a2.494 2.494 0 0 1-2.139-2.315c-.157-2.536-.81-4.508-1.84-5.552-2.206-2.241-2.678-5.686-1.148-8.377a14.173 14.173 0 0 1 2.323-3.034.43.43 0 0 0 .127-.353.455.455 0 0 0-.198-.344l-1.792-1.253a.47.47 0 0 0-.357-.078.423.423 0 0 0-.279.178c-.958 1.405-1.618 2.857-1.962 4.317-.73 3.095-3.313 5.425-6.428 5.797-2.31.277-4.376 1.589-6.139 3.901a2.494 2.494 0 0 1-3.967-3.023c2.575-3.378 5.865-5.394 9.514-5.83 1.06-.127 1.91-.908 2.165-1.99.485-2.057 1.392-4.07 2.697-5.983a5.379 5.379 0 0 1 3.501-2.274 5.422 5.422 0 0 1 4.114.898l1.792 1.253a5.444 5.444 0 0 1 2.308 4 5.418 5.418 0 0 1-1.577 4.318 9.075 9.075 0 0 0-1.506 1.965c-.44.774-.293 1.743.366 2.412 1.9 1.929 3.03 4.952 3.264 8.743a2.494 2.494 0 0 1-2.839 2.624" fill="#5BC434"/>
						</g>
					</svg>
					<h3>PEPPERS</h3>
					<div class="fade-wrap">
						<p>Are you looking to spice up your menu this fall? Thanks to the warm summer we've had our local farms have a plethora of amazing peppers right now across the capsicum spectrum! Simply add to your favorite dish to bring out the layers of flavor and add some complexity or let the pepper be the star of the show such as the Poblano.</p>
						<div class="fade"></div>
					</div>
					<a href="<?php echo get_home_url(); ?>/whats-in-season/" class="button is-ghost in-line continue">Continue Reading</a>
					<a href="<?php echo get_home_url(); ?>/online-ordering/" class="button is-secondary in-line">Order Online</a>
				</div>
				<div class="item">
					<svg width="87" height="90" xmlns="http://www.w3.org/2000/svg">
						<g fill="none" fill-rule="evenodd">
							<path d="M65.633 27.877a2.419 2.419 0 0 1-1.737-2.841l2.454-11.481a1.61 1.61 0 0 0-1.163-1.837 1.61 1.61 0 0 0-1.926 1.01l-3.615 11.169a2.412 2.412 0 0 1-3.046 1.556 2.418 2.418 0 0 1-1.556-3.046l3.624-11.197.015-.044c1.098-3.182 4.505-4.991 7.756-4.12 3.25.871 5.296 4.142 4.656 7.446l-.009.046-2.46 11.509a2.419 2.419 0 0 1-2.993 1.83" fill="#41C347" opacity=".66"/>
							<path d="M46.067 31.211c1.32-6.297 7.834-10.8 14.246-10.475 7.463.377 11.78 6.8 10.212 16.78-3.65 23.226-8.982 36.48-19.906 43.464-13.796 8.82-32.93 3.413-36.813-12.679-1.96-8.125 1.198-13.97 7.897-17.163 4.403-2.1 9.864-2.91 15.486-2.91 3.805 0 6.379-5.094 8.878-17.017zm5.872 1.231c-1.355 6.464-2.588 10.738-4.334 14.193-2.436 4.821-5.835 7.593-10.416 7.593-4.806 0-9.481.694-12.904 2.326-4.246 2.024-5.91 5.103-4.646 10.34 2.832 11.737 17.175 15.79 27.748 9.031 9-5.753 13.812-17.718 17.21-39.341 1.051-6.685-.957-9.672-4.587-9.856-3.585-.181-7.386 2.446-8.07 5.714z" fill="#EDD9AD" fill-rule="nonzero" opacity=".66"/>
						</g>
					</svg>
					<h3>WINTER SQUASH</h3>
					<div class="fade-wrap">
						<p>Don't worry, winter isn't here yet but the days are growing shorter and it's the time of year to enjoy our fall favorites. The winter squash's firm sweet flesh makes it a great substitute for starches and potatoes. Winter squash are nutrient dense with carotenes, vitamins, fiber, and complex carbohydrates, while being low cal, an added bonus for happy healthy guests!</p>
						<div class="fade"></div>
					</div>
					<a href="<?php echo get_home_url(); ?>/whats-in-season/" class="button is-ghost in-line continue">Continue Reading</a>
					<a href="<?php echo get_home_url(); ?>/online-ordering/" class="button is-secondary in-line">Order Online</a>
				</div>
			</div>
		</div>
	</div>
	<div class="section contents events">
		<div class="block-contain">
			<div class="overlay left"></div>
			<div class="block">
				<h3>EVENTS</h3>
				<h2>2018 FOOD SHOW</h2>
				<div class="event-card">
					<h4>March</h4>
					<p>12</p>
					<h5>2018</h5>
				</div>
				<div class="event-info">
					<p>Del Bene Produce is having a food show March 12, 2018 at The Eastern from 11-4pm.</p>
					<p>Learn about the services we offer and meet the farmers, vendors, and producers we partner with. Enjoy snack, chef demos, giveaways, and prizes. Hope to see you there!</p>
				</div>
				<a href="#" class="button is-tertiary" target="_blank">Register</a>
			</div>
		</div>
	</div>
	<div class="section">
		<?php get_template_part('template-parts/elements/sliders/testimonies'); ?>
	</div>
	<div class="section fp-auto-height">
		<?php get_template_part('template-parts/footer/footer'); ?>
	</div>
</div>