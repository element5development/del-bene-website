<?php 
/*-------------------------------------------------------------------

	PREVIEW CARD FOR SEARCH RESULT

------------------------------------------------------------------*/
?>

<article class="preview-search">
	<div>
		<h3><?php the_title(); ?></h3>
		<p><?php echo get_excerpt(150); ?></p>
	</div>
	<a href="<?php the_permalink(); ?>"></a>
</article>