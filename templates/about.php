<?php 
/*-------------------------------------------------------------------
    Template Name: About
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<?php get_template_part('template-parts/sections/info-banner'); ?>

<main>
	<a id="content" class="anchor"></a>
	<?php if( !empty(get_the_content()) ) { ?>
		<article class="default-contents">
			<?php the_content(); ?>
		</article>
	<?php } ?>
</main>

<?php get_template_part('template-parts/sections/two-column-section'); ?>

<?php get_template_part('template-parts/sections/staff-repeater'); ?>

<?php get_template_part('template-parts/elements/sliders/testimonies'); ?>
		
<?php get_template_part('template-parts/footer/footer'); ?>

<?php get_footer(); ?>