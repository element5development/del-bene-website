<?php 
/*-------------------------------------------------------------------
    Template Name: Homepage
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<section class="section contents we-feed">
	<div class="block-contain">
		<div class="overlay"></div>
		<div class="block">
			<h1><?php the_field('first_title'); ?></h1>
			<p><?php the_field('first_description'); ?></p>
			<?php $link = get_field('first_button'); ?>
			<?php if( $link ): ?>
				<a class="button is-secondary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
</section>
<section class="section contents why-del">
	<div class="block-contain">
		<div class="block">
			<h2><?php the_field('second_title'); ?></h2>
			<p><?php the_field('second_description'); ?></p>
			<?php $link = get_field('second_button'); ?>
			<?php if( $link ): ?>
				<a class="button is-tertiary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
</section>
<section class="section contents choose-del">
	<div class="overlay left"></div>
	<div class="overlay right"></div>
	<div class="block-contain">
		<div class="block">
			<h3><?php the_field('logo_section_title'); ?></h3>
			<h2><?php the_field('logo_section_sub_title'); ?></h2>
			<div class="companies">
				<div class="company">
					<img class="swap">
					<img class="swap">
				</div>
				<div class="company">
					<img class="swap">
					<img class="swap">
				</div>
				<div class="company">
					<img class="swap">
					<img class="swap">
				</div>
				<div class="company">
					<img class="swap">
					<img class="swap">
				</div>
				<div class="company">
					<img class="swap">
					<img class="swap">
				</div>
				<div class="company">
					<img class="swap">
					<img class="swap">
				</div>
			</div>
			<?php $link = get_field('logo_section_button'); ?>
			<?php if( $link ): ?>
				<a class="button is-primary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
</section>
<section class="section contents in-season">
	<div class="block-contain">
		<div class="block">
			<h2>WHAT�S IN SEASON?</h2>
			<a target="_blank" href="<?php the_field('market_report'); ?>" class="button is-primary is-ghost in-line">READ THE MARKET REPORT</a>
			<a target="_blank" href="<?php the_field('michigan_farms_list'); ?>" class="button is-primary is-ghost in-line">Michigan Farms List</a>
			<section class="produce-card-repeater">
				<div>
					<?php while ( have_rows('produce_card_repeater') ) : the_row(); ?>
						<div class="preview-produce-card item">
							<?php $image = get_sub_field('image'); ?>
							<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />

							<h3><?php the_sub_field('title'); ?></h3>

							<div class="fade-wrap">
								<p><?php the_sub_field('description'); ?></p>
								<div class="fade"></div>
							</div>
							<button class="is-ghost continue">Continue Reading</button>
							<a href="<?php echo get_home_url(); ?>/online-ordering/" class="button is-secondary in-line">Order Online</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		</div>
	</div>
</section>
<?php if( get_field('event_title') ): ?>
<section id="foodshow" class="section contents events">
	<div class="block-contain">
		<div class="overlay left"></div>
		<div class="block">
			<h3><?php the_field('events_section_title'); ?></h3>
			<h2><?php the_field('event_title'); ?></h2>
			<div class="event-card">
				<h4><?php the_field('month'); ?></h4>
				<p><?php the_field('day'); ?></p>
				<h5><?php the_field('year'); ?></h5>
			</div>
			<div class="event-info">
				<p><?php the_field('event_sub_title'); ?></p>
				<p><?php the_field('event_description'); ?></p>
			</div>
			<?php $link = get_field('event_button'); ?>
			<?php if( $link ): ?>
				<a class="button is-tertiary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_template_part('template-parts/elements/sliders/testimonies'); ?>
		
<?php get_template_part('template-parts/footer/footer'); ?>

<?php get_footer(); ?>