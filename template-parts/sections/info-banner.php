<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH INFO BANNER

\*----------------------------------------------------------------*/
?>

<section class="info-banner">
	<div>
		<?php if ( get_field('info_banner_title') ) : ?>
			<h2><?php the_field('info_banner_title'); ?></h2>
		<?php endif; ?>
		<div>
			<div>
				<?php the_field('wysiwyg_left'); ?>
			</div>
			<div>
				<?php the_field('wysiwyg_right'); ?>
			</div>
		</div>
		<?php $link = get_field('info_banner_button'); ?>
		<?php if( $link ): ?>
			<a class="button is-large" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
</section>