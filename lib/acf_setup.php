<?php

/*-----------------------------------------
  ENABLE ACF OPTIONS PAGE
-----------------------------------------*/
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
}

/*-----------------------------------------
		INCLUDE ACF INTO RELEVANSSI EXCERPTS
-----------------------------------------*/
add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
function custom_fields_to_excerpts($content, $post, $query) {

		/*
			Repeat the below code for each acf element you want to appear
			in the excerpt. Place them in the order you would want them
			to appear in the excerpt.

			There is no current way to include the repeater or flexiable
			content acf elements, keep this in mind when building pages.
		*/
		$custom_field = get_post_meta($post->ID, 'thank_you_message', true);
		$content .= " " . $custom_field;
		return $content;
}

/*-----------------------------------------
		ADD ICONS TO LEFT MENU
-----------------------------------------*/
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
	foreach( $items as &$item ) {
		$icon = get_field('menu_icon', $item);
		// append icon
		if( $icon ) {
			$item->title .= '<img src="'.$icon['url'].'" alt="'.$icon['alt'].'">';
		}
	}
	return $items;
}
?>