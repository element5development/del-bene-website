<?php 
/*----------------------------------------------------------------*\

	THREE COLUMN SECTION

\*----------------------------------------------------------------*/
?>

<section class="three-column">
	<div>
		<?php if ( get_field('three_column_title') ) : ?>
			<h2><?php the_field('three_column_title'); ?></h2>
		<?php endif; ?>

		<?php if ( get_field('three_column_description') ) : ?>
			<h6><?php the_field('three_column_description'); ?></h6>
		<?php endif; ?>

		<div>
			<div>
				<?php the_field('three_column_wysiwyg_left'); ?>
			</div>
			<div>
				<?php the_field('three_column_wysiwyg_center'); ?>
			</div>
			<div>
				<?php the_field('three_column_wysiwyg_right'); ?>
			</div>
		</div>
	</div>
</section>