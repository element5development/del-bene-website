<?php 
/*-------------------------------------------------------------------

	HEADER FOR SEARCH RESULTS

------------------------------------------------------------------*/
?>

<header class="page-title">
	<h1>Search results for "<?php echo get_search_query(); ?>"</h1>

	<div class="overlay"></div>
</header>