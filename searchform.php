<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<form role="search" method="get" action="<?php echo get_site_url(); ?>">
	<input type="search" placeholder="<?php echo esc_attr_x( 'Search…', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
	<button type="submit" class="is-small is-borderless">
		<svg id="search" viewBox="0 0 28 28">
			<path d="M20 17.64l8 8L25.6 28l-8-8v-1.23l-.45-.45a10 10 0 0 1-6.76 2.47 10.13 10.13 0 0 1-7.39-3 9.92 9.92 0 0 1-3-7.36A10.05 10.05 0 0 1 3 3a10.05 10.05 0 0 1 7.39-3 9.92 9.92 0 0 1 7.36 3 10.13 10.13 0 0 1 3 7.39 10 10 0 0 1-2.47 6.76l.45.45zm-9.61 0a7 7 0 0 0 5.11-2.1 7 7 0 0 0 2.1-5.11 6.94 6.94 0 0 0-2.1-5.1 7 7 0 0 0-5.11-2.1 6.94 6.94 0 0 0-5.1 2.1 6.94 6.94 0 0 0-2.1 5.1 7 7 0 0 0 2.1 5.11 6.94 6.94 0 0 0 5.14 2.1z" />
		</svg>
	</button>
</form>