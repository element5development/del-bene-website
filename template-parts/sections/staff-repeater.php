<?php 
/*----------------------------------------------------------------*\

	STAFF REPEATER SECTION

\*----------------------------------------------------------------*/
?>

<?php if( get_field('show_section') ): ?>
<section class="staff-repeater">
	<div>
		<?php if ( get_field('staff_repeater_title') ) : ?>
			<h2><?php the_field('staff_repeater_title'); ?></h2>
		<?php endif; ?>
		
		<?php while ( have_rows('staff_repeater') ) : the_row(); ?>
			<article class="preview-staff">
				<?php $image = get_sub_field('headshot'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
				<div class="member-info">
					<h5><?php the_sub_field('name'); ?></h5>
					<p class="position"><?php the_sub_field('position'); ?></p>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>