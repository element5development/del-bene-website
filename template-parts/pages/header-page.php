<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<header class="page-title">
	<h1><?php if ( get_field('page_title') ) : the_field('page_title'); else : the_title(); endif; ?></h1>

	<?php if ( get_field('title_description') ) : ?>
		<p><?php the_field('title_description'); ?></p>
	<?php endif; ?>

	<?php $link = get_field('title_button'); ?>
	<?php if( $link ): ?>
		<a class="button is-secondary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
	<?php endif; ?>

	<div class="overlay"></div>
</header>