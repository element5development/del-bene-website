<?php 
/*----------------------------------------------------------------*\

	TWO COLUMN SECTION

\*----------------------------------------------------------------*/
?>

<section class="two-column">
	<div>
		<?php $image = get_field('top_image'); ?>
		<?php if( $image ): ?>
			<img src="<?php echo $image['sizes']['xlarge']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php endif; ?>

		<?php if ( get_field('two_column_title') ) : ?>
			<h2><?php the_field('two_column_title'); ?></h2>
		<?php endif; ?>

		<?php if ( get_field('two_column_description') ) : ?>
			<p><?php the_field('two_column_description'); ?></p>
		<?php endif; ?>

		<div>
			<div>
				<?php the_field('two_column_wysiwyg_left'); ?>
			</div>
			<div>
				<?php the_field('two_column_wysiwyg_right'); ?>
			</div>
		</div>

		<?php $link = get_field('two_column_button'); ?>
		<?php if( $link ): ?>
			<a class="button is-secondary is-large" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
</section>