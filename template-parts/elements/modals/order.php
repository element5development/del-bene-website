<div id="order-modal" class="is-hidden">
	<h3>Order Online</h3>
	<p>Del Bene Produce is excited to offer online ordering for all of our customers.</p>
	<a href="https://delbene.freshbyte.store/login" class="button is-primary">Customer Login</a>
	<a href="<?php echo get_home_url(); ?>/sign-up/">Not enrolled? Sign Up Now</a>
</div>