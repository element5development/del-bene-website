<?php 
/*----------------------------------------------------------------*\

	PRODUCT REPEATER SECTION

\*----------------------------------------------------------------*/
?>

<?php if( have_rows('product_repeater') ): ?>
<section class="product-repeater">
	<div>
		<?php while ( have_rows('product_repeater') ) : the_row(); ?>
			<article class="preview-product">
				<div class="product-image">
					<?php $image = get_sub_field('image'); ?>
					<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				<div class="product-info">
					<h3><?php the_sub_field('title'); ?></h3>
					<p class="description"><?php the_sub_field('description'); ?></p>
					<?php $link = get_sub_field('button'); ?>
					<?php if( $link ): ?>
						<a class="button is-secondary is-large" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>