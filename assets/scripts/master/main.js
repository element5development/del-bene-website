var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*------------------------------------------------------------------
		INPUT ADDING AND REMVOING CLASSES
	------------------------------------------------------------------*/
	$('input:not([type=checkbox]):not([type=radio])').focus(function () {
		$(this).addClass('is-activated');
	});
	$('textarea').focus(function () {
		$(this).addClass('is-activated');
	});
	$('select').focus(function () {
		$(this).addClass('is-activated');
	});
	/*------------------------------------------------------------------
  	TESTIMONY COVERFLOW
	------------------------------------------------------------------*/
	$('.testimonies-block').flipster({
		itemContainer: '.testimonies',
		itemSelector: '.testimony',
		loop: 1,
		start: 'center',
		style: 'infinite-carousel',
		spacing: 0,
		scrollwheel: false,
		touch: true,
		buttons: false,
	});
	/*------------------------------------------------------------------
  	FULL SCREEN SLIDER
	------------------------------------------------------------------*/
	// $('#fullscreen').fullpage({
	// 	navigation: true,
	// 	navigationPosition: 'right',
	// });
	/*------------------------------------------------------------------
  	CLIENTS
	------------------------------------------------------------------*/
	var imgArray = [
		"/wp-content/themes/starting-point/dist/images/client_chartreuse.png",
		"/wp-content/themes/starting-point/dist/images/client_delaware.png",
		"/wp-content/themes/starting-point/dist/images/client_hotel.png",
		"/wp-content/themes/starting-point/dist/images/client_johnny.png",
		"/wp-content/themes/starting-point/dist/images/client_mabel.png",
		"/wp-content/themes/starting-point/dist/images/client_midfield.png",
		"/wp-content/themes/starting-point/dist/images/client_selden.png",
		"/wp-content/themes/starting-point/dist/images/client_testabarra.png",
		"/wp-content/themes/starting-point/dist/images/client_WAB.png",
		"/wp-content/themes/starting-point/dist/images/client_wright.png",
		"/wp-content/themes/starting-point/dist/images/client_fishbones.png"
	];

	function loadImages() {
		$('.company').each(function (i) {
			var newImage = imgArray.splice(0, 1)[0];
			$(this).find('.swap').attr('src', newImage);
		});
	}

	function swapImage() {
		var imgChoice = Math.floor(Math.random() * imgArray.length);
		var newImage = imgArray.splice(imgChoice, 1)[0];
		var target = Math.floor(Math.random() * $('.company').length);
		var target = target + 1;
		var mainImg = $('.company:nth-child(' + target + ') .swap').eq(1);
		var swapImg = $('.company:nth-child(' + target + ') .swap').eq(0);
		swapImg.show().attr('src', newImage);
		imgArray.push(mainImg.attr('src'));
		swapImg.fadeIn(2000);
		mainImg.fadeOut(2000, function () {
			swapImg.insertAfter(mainImg);
			swapImage();
		});
	}
	loadImages();
	swapImage();
	/*------------------------------------------------------------------
  	STICKY NAV
	------------------------------------------------------------------*/
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll > 42) {
			$('.top-bar').addClass('is-hidden');
			$('.nav-primary').addClass('is-stuck');
			$('nav.nav-primary a.brand').addClass('scroll');
		} else {
			$('.top-bar').removeClass('is-hidden');
			$('.nav-primary').removeClass('is-stuck');
			$('nav.nav-primary a.brand').removeClass('scroll');
		}
	});
	/*------------------------------------------------------------------
  	TOP BAR CLOSE
	------------------------------------------------------------------*/
	if (readCookie('topClosed') === 'true') {
		$('.top-bar').addClass('is-closed');
		$('.nav-primary').addClass('top-closed');
		$('.top-bar').addClass('load-closed');
		$('.nav-primary').addClass('load-top-closed');
	} else {
		$('.top-bar').addClass('is-open');
		$('.nav-primary').addClass('top-open');
		createCookie('topClosed', 'true');
	}
	$('.top-bar button').click(function () {
		$('.top-bar').addClass('is-closed');
		$('.nav-primary').addClass('top-closed');
		$('.top-bar').removeClass('is-open');
		$('.nav-primary').removeClass('top-open');
		createCookie('topClosed', 'true');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*------------------------------------------------------------------
  	MOBILE MENU
	------------------------------------------------------------------*/
	$('.menu-toggle').click(function () {
		if ($(window).width() < 768) {
			return false;
		}
	});
	/*----------------------------------------------------------------*\
		MOBILE NAV
	\*----------------------------------------------------------------*/
	$('.menu-toggle svg#menu').click(function () {
		$('nav.nav-primary-left').addClass('is-active');
		$('nav.nav-primary a svg#menu').addClass('is-hidden');
		$('nav.nav-primary a svg#close').addClass('is-active');
	});
	$('nav.nav-primary a svg#close').click(function () {
		$('nav.nav-primary-left').removeClass('is-active');
		$('nav.nav-primary a svg#menu').removeClass('is-hidden');
		$('nav.nav-primary a svg#close').removeClass('is-active');
	});
	/*----------------------------------------------------------------*\
		LEFT NAV
	\*----------------------------------------------------------------*/
	$('nav.nav-primary-left ul li a').hover(function () {
		if ($(window).width() > 767) {
			$('nav.nav-primary-left ul').addClass("is-hover");
		}
	}, function () {
		if ($(window).width() > 767) {
			$('nav.nav-primary-left ul').removeClass("is-hover");
		}
	});
	/*------------------------------------------------------------------
  	CONTINUE READING
	------------------------------------------------------------------*/
	$('.preview-produce-card button.continue').click(function () {
		$(this).toggleClass('is-more');
		$(this).parent().toggleClass('is-more');
	});
	/*----------------------------------------------------------------*\
		SITE SEARCH
	\*----------------------------------------------------------------*/
	$('nav.nav-primary button').click(function () {
		$('nav.nav-primary form').addClass('is-active');
		$('nav.nav-primary form input').focus();
	});
	$('html').click(function (event) {
		if ($(event.target).closest('nav.nav-primary button, nav.nav-primary form').length === 0) {
			$('nav.nav-primary form').removeClass("is-active");
		}
	});

});