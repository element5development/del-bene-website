<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main>
	<a id="content" class="anchor"></a>

	<header class="page-title">
		<div class="error-message">
			<h1>Oops!</h1>
			<p>We can't seem to find the page you're looking for.</p>
			<a href="<?php echo get_home_url(); ?>" class="button is-secondary">Back to home</a>
			<div class="overlay"></div>
		</div>
	</header>
</main>

<?php get_template_part('template-parts/footer/footer'); ?>

<?php get_footer(); ?>