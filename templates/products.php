<?php 
/*-------------------------------------------------------------------
    Template Name: Products
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<?php get_template_part('template-parts/sections/info-banner'); ?>

<main>
	<a id="content" class="anchor"></a>
	<?php if( !empty(get_the_content()) ) { ?>
		<article class="default-contents">
			<?php the_content(); ?>
		</article>
	<?php } ?>
</main>

<?php get_template_part('template-parts/sections/product-repeater'); ?>
		
<?php get_template_part('template-parts/footer/footer'); ?>

<?php get_footer(); ?>