<?php 
/*-------------------------------------------------------------------
    Template Name: Style Guide
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<main>
	<a id="content" class="anchor"></a>
	<article class="default-contents">
		<h1>This is an example of an h1</h1>
		<h3>This is an example of an Subhead</h3>

		<h2>This is an example of an h2</h2>
		<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Inline Link, a pharetra augue. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue.</p>
		<div class="wysiwyg-two-col">
			<div>
				<ul>
					<li>This is a unordered list item</li>
					<li>This is a unordered list item</li>
					<li>This is a unordered list item</li>
					<li>This is a unordered list item</li>
					<li>This is a unordered list item</li>
				</ul>
			</div>
			<div>
				<ol>
					<li>This is a ordered list</li>
					<li>This is a ordered list</li>
					<li>This is a ordered list</li>
					<li>This is a ordered list</li>
					<li>This is a ordered list</li>
				</ol>
			</div>
		</div>
		<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue.</p>
		<hr>
		<h3>This is an example of an h3</h3>
		<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna mollis ornare vel eu leo. This is a text link cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue.</p>
		<blockquote><p>“This is an example of blockquote text. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Lorem ipsum dolor sit amet, consectetur adipiscing elit.”</p></blockquote>
		<div class="wysiwyg-two-col">
			<div>
				<h4>This is an example of an h4</h4>
				<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui.</p>
			</div>
			<div>
				<h4>This is an example of an h4</h4>
				<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui.</p>
			</div>
		</div>
		<h5>This is an example of an h5</h5>
		<h6>This is an example of an h6</h6>
		<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam quis risus eget urna mollis ornare vel eu leo. This is a text link cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue.</p>
		<div class="wysiwyg-four-col">
			<div>
				<button>Button Style 1</button>
				<button class="is-hover">Button Hover 1</button>
			</div>
			<div>
				<button class="is-secondary">Button Style 2</button>
				<button class="is-secondary is-hover">Button Hover 2</button>
			</div>
			<div>
				<button class="is-tertiary">Button Style 3</button>
				<button class="is-tertiary is-hover">Button Hover 3</button>
			</div>
		</div>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]') ?>
	</article>
</main>

<?php get_template_part('template-parts/footer/footer'); ?>

<?php get_footer(); ?>