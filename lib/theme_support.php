<?php

/*-----------------------------------------
  INCLUDE CSS AND JS
-----------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
	wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');
	wp_enqueue_style('fonts', 'https://use.typekit.net/wjx5mbl.css', array(), '1.1', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array (), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array ( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*-----------------------------------------
  THEME STYLES IN THE VISUAL EDITOR
-----------------------------------------*/
add_editor_style('/assets/styles/main.css');

/*-----------------------------------------
  Enable Excerpts
-----------------------------------------*/
add_post_type_support( 'page', 'excerpt' );
add_post_type_support( 'post', 'excerpt' );

/*-----------------------------------------
  Enable Post Thumbnails
-----------------------------------------*/
add_theme_support('post-thumbnails');

/*-----------------------------------------
  Enable HTML5 Markup Support
-----------------------------------------*/
add_theme_support('html5', array(
	'caption', 
	'comment-form', 
	'comment-list', 
	'gallery', 
	'search-form'
));

/*-----------------------------------------
  CHANGE SEARCH POST COUNT
-----------------------------------------*/
function change_wp_search_size($query) {
	if ( $query->is_search ) 
		$query->query_vars['posts_per_page'] = 8; 
	return $query; 
}
add_filter('pre_get_posts', 'change_wp_search_size'); 

/*----------------------------------------------------------------*\
	ADD & CUSTOMIZE EXCERPTS
\*----------------------------------------------------------------*/
add_post_type_support( 'page', 'excerpt' );
function get_excerpt($limit, $source = null){
	$excerpt = get_the_excerpt();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'...';
	return $excerpt;
}